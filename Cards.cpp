#include "Cards.h"
#include <cstdlib>
#include <iostream>
#include <iomanip>

/* *************************************************
 Card class
 ************************************************* */

/*
 Default constructor for the Card class. It could give repeated cards.*/
Card::Card(){
    int r = 1 + rand() % 4;
    switch (r) {
        case 1: suit = OROS; break;
        case 2: suit = COPAS; break;
        case 3: suit = ESPADAS; break;
        case 4: suit = BASTOS; break;
        default: break;
    }
    
    r = 1 + rand() % 10;
    switch (r) {
        case  1: rank = AS; break;
        case  2: rank = DOS; break;
        case  3: rank = TRES; break;
        case  4: rank = CUATRO; break;
        case  5: rank = CINCO; break;
        case  6: rank = SEIS; break;
        case  7: rank = SIETE; break;
        case  8: rank = SOTA; break;
        case  9: rank = CABALLO; break;
        case 10: rank = REY; break;
        default: break;
    }
}

// Accessor: returns a string with the suit of the card in Spanish
string Card::get_spanish_suit() const {
    string suitName;
    switch (suit) {
        case OROS:
            suitName = "oros";
            break;
        case COPAS:
            suitName = "copas";
            break;
        case ESPADAS:
            suitName = "espadas";
            break;
        case BASTOS:
            suitName = "bastos";
            break;
        default: break;
    }
    return suitName;
}

// Accessor: returns a string with the rank of the card in Spanish
string Card::get_spanish_rank() const {
    string rankName;
    switch (rank) {
        case AS:
            rankName = "As";
            break;
        case DOS:
            rankName = "Dos";
            break;
        case TRES:
            rankName = "Tres";
            break;
        case CUATRO:
            rankName = "Cuatro";
            break;
        case CINCO:
            rankName = "Cinco";
            break;
        case SEIS:
            rankName = "Seis";
            break;
        case SIETE:
            rankName = "Siete";
            break;
        case SOTA:
            rankName = "Sota";
            break;
        case CABALLO:
            rankName = "Caballo";
            break;
        case REY:
            rankName = "Rey";
            break;
        default: break;
    }
    return rankName;
}

// Accessor: returns a string with the suit of the card in English
string Card::get_english_suit() const {
    string suitNameeng;
    switch (suit) {
        case OROS:
            suitNameeng = "Golds";
            break;
        case COPAS:
            suitNameeng = "Cups";
            break;
        case ESPADAS:
            suitNameeng = "Swords";
            break;
        case BASTOS:
            suitNameeng = "Clubs";
            break;
        default: break;
    }
    return suitNameeng;
}

// Accessor: returns a string with the rank of the card in English
string Card::get_english_rank() const {
    string rankNameeng;
    switch (rank) {
        case AS:
            rankNameeng = "Ace";
            break;
        case DOS:
            rankNameeng = "Two";
            break;
        case TRES:
            rankNameeng = "Three";
            break;
        case CUATRO:
            rankNameeng = "Four";
            break;
        case CINCO:
            rankNameeng = "Five";
            break;
        case SEIS:
            rankNameeng = "Six";
            break;
        case SIETE:
            rankNameeng = "Seven";
            break;
        case SOTA:
            rankNameeng = "Jack";
            break;
        case CABALLO:
            rankNameeng = "Horse";
            break;
        case REY:
            rankNameeng = "King";
            break;
    }
    return rankNameeng;
}

// Assigns a numerical value to card based on rank.
int Card::get_rank() const {
    return static_cast<int>(rank) + 1 ;
}

// Comparison operator for cards
bool Card::operator < (Card card2) const {
    return rank < card2.rank; // Returns TRUE if card1 < card2
}



/* *************************************************
 Hand class
 ************************************************* */
Hand::Hand()
{
    vector <Card> set;
}

double Hand::get_points() const
{
    return points; 
}

void Hand::reset_points()
{
    points = 0.0;
}

void Hand::update_hand(Card card)
{
    hand.push_back(card);
    if(card.get_rank() <= 7)
    {
        points += card.get_rank();
    }
    else if (card.get_rank() > 7)
    {
        points += 1.5;
    } 
}

void Hand::print()
{
    for(int i = 0 ; i < hand.size(); i++)
    {
        cout << left << setw(4)  << ""
             << left << setw(20) << hand[i].get_spanish_rank() + " " + hand[i].get_spanish_suit()
             << left << setw(13) << "(" + hand[i].get_english_rank() + " " + hand[i].get_english_suit() + ")"
             << "\n\n";*/
    } 
}


/* *************************************************
 Player class
 ************************************************* */
//Assigns 100 pesos for one play
Player::Player(int d)
{
    dollars = 100; 
}

void Player::set_bet(int b)
{
    bet = b;
}

int Player::get_bet()
{
    return bet;
}

int Player::get_total_amount()
{
    return dollars;
}

void Player::game_winner(Hand player, Hand dealer)
{
    string result_of_round;
    if((player.get_points() < 7.5 && player.get_points() > dealer.get_points()) || dealer.get_points() > 7.5)
    {
        //player wins in these scenarios
        result_of_round = "win";
        update_total_amount(result_of_round, bet);
        cout << "You win " << bet << "\n";
    }
    else
    if((dealer.get_points() < 7.5 && dealer.get_points() > player.get_points()) || player.get_points() > 7.5)
    {
        //player loses in these scenarios
        result_of_round = "lose";
        update_total_amount(result_of_round, bet);
        cout << "You lose " << bet << "\n";
    }
    else
    if((player.get_points() && dealer.get_points()) > 7.5)
    {
        //house advantage - player loses
        result_of_round = "lose";
        update_total_amount(result_of_round, bet);
        cout << "House Advantage. You lose " << bet << "\n";
    }
    else
    {
        cout << "Nobody wins! \n";
    }
}

void Player::update_total_amount(string result_ofround, int bet_dollars)
{
    if(result_ofround == "win")
    {
        dollars += bet_dollars;
    }
    else{
        dollars -= bet_dollars;
    }
}
