#Homework 1 - Alyson Jimenez
Included: Cards.h, Cards.cpp, siete-y-medio.cpp 

This was my first attempt at working with github and I did have a lot of issues. But I was able to do it successfully.

First, in Xcode, I would commit successfully but trying to branch I would try to checkout and it would elimate what I did before. What I realized I did was unsuccessfully comit before branching out and merging so all the previous info would be deleted. After multiple trial and errors, I got the hang of branching and merging. 
I had to restart the project multiple times still when working with BitBucket because instead of pushing the file into the repository, I was actually cloning the empty repository. I eventually decided to test a copy of the project so I wouldn't have to do it again multiple times.

The project itself was simple and required some playing around with to the organization that I wanted to. My branch was testing the idea of putting a void function in the main but therefore making the main file the one that does all the work. I realized how inefficient and confusing it was and overall decided it wasn't the best idea.
Another thing about the project that did confuse me was the importance of what the Hand class and the Player class did. Originally, I was putting all the functions regaring points in the Hand but nothing was working out until I rethought the intentions of each class and found that putting the funciton that determines the game winner in Player was a lot easier. 

Overall, I found this project to be tedious but I did learn a lot from it. I believed that I was able to program well but this project showed me I still had much more to learn. Using git and learning of its important in class and in practice wil definitely help me in the long run. 