#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <iomanip>
#include "Cards.h"

using namespace std;

void printCard(Card card)
{
    cout << left << setw(4)  << ""
         << left << setw(20) << card.get_spanish_rank() + " " + card.get_spanish_suit()
         << left << setw(13) << "(" + card.get_english_rank() + " " + card.get_english_suit() + ")"
         << "\n\n";
}

int main()
{
    //Player's turn
    Player one(100);
    srand(time(NULL));
    
    while(one.get_total_amount() > 0)
    {
        Hand players_hand;
        int bet;
        string answerdraw = "y";
        
        cout << "You have $" << one.get_total_amount() << ". Enter bet: ";
        cin >> bet;
        if(bet > one.get_total_amount())
        {
            cout << "Sorry, that was over the amount you have, enter a new bet: ";
            cin >> bet;
        }
        cout << "\n";
        one.set_bet(bet);
        players_hand.reset_points();
        
        while(answerdraw == "y" && players_hand.get_points() <= 7.5)
        {
            Card players_draw_card;
            players_hand.update_hand(players_draw_card);
            
            cout << "New Card: \n";
            printCard(players_draw_card);
            cout << "Your cards: \n";
            players_hand.print();
            
            cout << "Your total is " << players_hand.get_points() << ". Do you want another card(y/n)? ";
            cin >> answerdraw;
            cout << "\n";
        }
        
        //Dealer's turn
        Hand dealers_hand;
        dealers_hand.reset_points();
        
        while(dealers_hand.get_points() <= 5.5)
        {
            Card dealers_draw_card;
            dealers_hand.update_hand(dealers_draw_card);
            cout << "Dealer cards: \n";
            dealers_hand.print();
            cout << "The dealer's total is " << dealers_hand.get_points() << ".\n\n";
        }
        
        one.game_winner(players_hand, dealers_hand);
    }
    
    cout << "You have $0. GAME OVER! \nCome back when you have more money! \nBye!\n";
    
    return 0;
}
