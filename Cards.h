#ifndef CARDS_H
#define CARDS_H

#include <string>
#include <vector>
#include <fstream>

using namespace std;

enum suit_t {OROS, COPAS, ESPADAS, BASTOS};
enum rank_t {AS, DOS, TRES, CUATRO, CINCO, SEIS, SIETE, SOTA=9, CABALLO=10, REY=11};

class Card {
public:
    // Constructor assigns random rank & suit to card.
    Card();
    
    // Accessors
    string get_spanish_suit() const;
    string get_spanish_rank() const;
    string get_english_suit() const;
    string get_english_rank() const;
    
    // Converts card rank to number
    int get_rank() const;
    
    // Compare rank of two cards. (no use in code)
    bool operator < (Card card2) const;
    
private:
    suit_t suit;
    rank_t rank;
};

class Hand {
public:
    Hand();
    
    double get_points() const;
    void   reset_points();
    void   update_hand(Card card); 
    void   print();
    
private:
    vector<Card> hand;
    double       points;
};

class Player {
public:
    Player(int d); // dollars for use of betting
    
    void set_bet(int b);
    int  get_bet();
    int  get_total_amount();
    void game_winner(Hand player, Hand dealer);
    void update_total_amount(string result_ofround, int bet_dollars);
    
private:
    Hand   hand;
    int    dollars;
    int    bet;
    string result;
};

#endif
